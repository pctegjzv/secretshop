mkdir -p dist
rm -rf node_modules
if [ -d "node_modules_old" ]; then
  cp -Rf node_modules_old node_modules
else
  npm install
  npm install raml2html@3.0.1
  #npm install raml2html@3.0.1 --registry=https://registry.npm.taobao.org
  cp -Rf node_modules node_modules_old
fi
echo "building raml 0.8..."
npm run build_old_raml

rm -rf node_modules
if [ -d "node_modules_new" ]; then
  cp -Rf node_modules_new node_modules
else
  npm install
  npm install raml2html@6
  # npm install raml2html@6 --registry=https://registry.npm.taobao.org
  cp -Rf node_modules node_modules_new
fi
echo "building raml 1.0..."
npm run build_new_raml
