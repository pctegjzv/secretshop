#### Common Error Codes

* `invalid_args`: 传入参数不对
* `kubernetes_error`: kubernetes返回的错误


#### 招商特定

* `resource_not_available`: 配额检查不过
* `resource_quota_create_error`: 创建配额失败
* `secret_create_error`: 创建secret失败
* `jfrog_namespace_create_error`: 创建jfrog namespace失败
* `sa_add_secret_error`: 更新ServiceAccount失败
