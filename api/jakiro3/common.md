### common api info

#### list query params

* org_name: optinal. If provided, use as user's namespace
* cluster: required if this resource belong to a cluster
* namespace: required if this resource belong to a cluster namespace
* name: search resource by name (except for pods)

### uuid field in request body
cluster.uuid和namespace.uuid在post body里面是可选的

cluster.uuid and namespace.uuid is optional in request body

### patch 
PATCH method only need to send the field to be updated.